package com.example.demo;

import com.example.demo.entity.Category;
import com.example.demo.entity.Customer;
import com.example.demo.entity.Product;
import com.example.demo.entity.Product_Customer;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.Date;

@SpringBootApplication
@Transactional
public class DemoApplication implements ApplicationRunner {
    @PersistenceContext
    private EntityManager entityManager;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {


        Customer customer = new Customer("virak","01234567","M",10,"PP");
        Category category = new Category("coke");
        entityManager.persist(category);
        Date date = new Date();
        Product product = new Product("Kheang","1243",12.0,10,date,category);
        entityManager.persist(product);
        entityManager.persist(customer);
        entityManager.persist(new Product_Customer(product,customer,12,12.0,20.0));


        Customer customer1 = new Customer("virak1","01234567","F",10,"PP");
        Date date1 = new Date();
        Product product1 = new Product("Kheang1","4321",12.0,10,date1,category);
        entityManager.persist(product1);
        entityManager.persist(customer1);
        entityManager.persist(new Product_Customer(product1,customer1,12,12.0,20.0));


        Customer customer2 = new Customer("virak2","01234567","M",10,"PP");
        Date date2 = new Date();
        Product product2 = new Product("Kheang2","1243",12.0,10,date2,category);
        entityManager.persist(product2);
        entityManager.persist(customer2);
        entityManager.persist(new Product_Customer(product2,customer2,12,12.0,20.0));

        Customer customer3 = new Customer("virak3","01234567","M",10,"PP");
        Date date3 = new Date();
        Product product3 = new Product("Kheang3","1243",12.0,10,date3,category);
        entityManager.persist(product3);
        entityManager.persist(customer3);
        entityManager.persist(new Product_Customer(product3,customer3,12,12.0,20.0));

        Customer customer4 = new Customer("virak4","01234567","M",10,"PP");
        Date date4 = new Date();
        Product product4 = new Product("Kheang4","1243",12.0,10,date4,category);
        entityManager.persist(product4);
        entityManager.persist(customer4);
        entityManager.persist(new Product_Customer(product4,customer4,12,12.0,20.0));

        Customer customer5 = new Customer("virak5","01234567","M",10,"PP");
        Date date5= new Date();
        Product product5 = new Product("Kheang5","1243",12.0,10,date5,category);
        entityManager.persist(product5);
        entityManager.persist(customer5);
        entityManager.persist(new Product_Customer(product5,customer5,12,12.0,20.0));


    }
}
