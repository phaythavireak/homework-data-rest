package com.example.demo.entity;


import javax.persistence.*;
import java.util.Date;

@Entity(name = "Product_Entity")
@Table(name = "Product")
public class Product {



    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String code;
    @Column(name = "stock_price",columnDefinition = "Decimal(10,2)")
    private Double stockprice;
    private Integer stock_qty;
    private Date import_date;
    @OneToOne
    private Category category;

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", stockprice=" + stockprice +
                ", stock_qty=" + stock_qty +
                ", import_date=" + import_date +
                ", category=" + category +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getStockprice() {
        return stockprice;
    }

    public void setStockprice(Double stockprice) {
        this.stockprice = stockprice;
    }

    public Integer getStock_qty() {
        return stock_qty;
    }

    public void setStock_qty(Integer stock_qty) {
        this.stock_qty = stock_qty;
    }

    public Date getImport_date() {
        return import_date;
    }

    public void setImport_date(Date import_date) {
        this.import_date = import_date;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Product() {

    }

    public Product(String name, String code, Double stockprice, Integer stock_qty, Date import_date, Category category) {

        this.name = name;
        this.code = code;
        this.stockprice = stockprice;
        this.stock_qty = stock_qty;
        this.import_date = import_date;
        this.category = category;
    }
}
