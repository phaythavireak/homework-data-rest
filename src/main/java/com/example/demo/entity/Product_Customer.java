package com.example.demo.entity;

import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
public class Product_Customer implements Serializable {


    @Id
    @ManyToOne
    @JoinColumn(name = "pro_id")
    private Product product;
    @Id
    @ManyToOne
    @JoinColumn(name = "cust_id")
    private Customer customer;
    private Integer qty;
    @Column(name = "price",columnDefinition = "Decimal(10,2)")
    private Double price;
    @Column(name = "amount",columnDefinition = "Decimal(10,2)")
    private Double amount;


    @Override
    public String toString() {
        return "Product_Customer{" +
                "product=" + product +
                ", customer=" + customer +
                ", qty=" + qty +
                ", price=" + price +
                ", amount=" + amount +
                '}';
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Product_Customer() {

    }

    public Product_Customer(Product product, Customer customer, Integer qty, Double price, Double amount) {

        this.product = product;
        this.customer = customer;
        this.qty = qty;
        this.price = price;
        this.amount = amount;
    }
}
