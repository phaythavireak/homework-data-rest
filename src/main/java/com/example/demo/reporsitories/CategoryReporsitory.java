package com.example.demo.reporsitories;

import com.example.demo.entity.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryReporsitory extends CrudRepository<Category,Integer> {
}
