package com.example.demo.reporsitories;

import com.example.demo.entity.Customer;
import com.example.demo.entity.Product_Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;


@RestResource(path = "/customer",rel = "customer")
@RepositoryRestResource(itemResourceRel = "Khmer",collectionResourceRel = "sdach"
)
public interface CustomerReporsitory extends CrudRepository<Customer,Integer> {




    Customer findCustomerByName(@Param("name") String name);
    Customer findCustomerByAge(Integer age);








}
