package com.example.demo.reporsitories;

import com.example.demo.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProductReporsitory extends CrudRepository<Product,Integer> {
}



